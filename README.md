## Alfabank REST API integration

### Installation
Install this package with composer:
```
composer require foxtes/alfabank-rest
```

### Usage
```
use Foxtes\Alfabank\AlfaBankClient;
use Foxtes\Alfabank\Models\Request\Order;
use Foxtes\Alfabank\Models\Request\OrderPosition;
```

Create new order in bank
```
$bank = new AlfaBankClient('login-api', 'password-api', true);
// register order
$res = $bank->getPaymentUrl(new Order('123', 'test', 500, 'descr'), 'http://app.com');
```

### Fiscalization (optional)
 
If you use fiscalization via Alfabank, you need send items in order

```
$order = new Order('123', 'test', 500, 'descr', 'customer@email.com', [
    new OrderPosition('123', 'test', 500, 'descr')
]);
$res = $bank->getPaymentUrl($order, 'http://app.com');

```

Check order status
```
// get order status
$status = $bank->checkStatus('alfabank order id', 'order id in your system');
```

### Logging actions (optional)

You can use custom logger for this package. Just create logger class implement from package LoggerContract interface.

```
use Foxtes\Alfabank\Logger\LoggerContract;

class MyLog implements LoggerContract
{
    public static function push(...);
}

$logger = new MyLog();

$bank = new AlfaBankClient('login-api', 'password-api', true, $logger);

``` 

### Test cards for check integration

https://pay.alfabank.ru/ecommerce/instructions/test_cards.pdf

### Telegram channel about PHP packages
Subscribe - https://t.me/php_package
