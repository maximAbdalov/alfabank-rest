<?php

namespace Foxtes\Alfabank\Logger;

interface LoggerContract
{
	public static function push($orderId, $paymentId, $typeAction, array $response, $fullJsonResponse = "");
}
