<?php namespace Foxtes\Alfabank\Models\Response;

use Foxtes\Alfabank\Models\BankModel;

class CheckStatusResponse extends BankModel
{
	public $status;

	public $statusCode;

	public $statusText;

	public $orderId;

	public $orderStatus;

	/**
	 * Define order payment status
	 * @var
	 */
	public $orderStatusCode;

	public $jsonResponse;
}
