<?php

namespace Foxtes\Alfabank\Models\Response;

use Foxtes\Alfabank\Models\BankModel;

class PaymentUrlResponse extends BankModel
{
	public $error;

	public $paymentUrl;

	public $bankOrderId;

	public $errorCode;

	public $errorMessage;

	public $jsonResponse;
}
